/* Copyright 2015-2017 Jack Humbert
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include QMK_KEYBOARD_H
#include "muse.h"


// Macro
// https://beta.docs.qmk.fm/using-qmk/advanced-keycodes/feature_macros

enum custom_keycodes {
  QMKMACROTEST = SAFE_RANGE,
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case QMKMACROTEST:
      if (record->event.pressed) {
        // when keycode QMKMACROTEST is pressed
        SEND_STRING("This is a Macro");
      } else {
        // when keycode QMKMACROTEST is released
      }
      break;

  }
  return true;
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

    [0] = LAYOUT_planck_2x2u(
        LT(2, KC_TAB),   KC_Q,    KC_W,     KC_E,   KC_R,   KC_T,   KC_Y,   KC_U,   KC_I,     KC_O,    KC_P,     KC_BSPC, 
        KC_ESC,          KC_A,    KC_S,     KC_D,   KC_F,   KC_G,   KC_H,   KC_J,   KC_K,     KC_L,    KC_SCLN,  KC_QUOT,
        KC_LSFT,         KC_Z,    KC_X,     KC_C,   KC_V,   KC_B,   KC_N,   KC_M,   KC_COMM,  KC_DOT,  KC_SLSH,  KC_LPRN, 
        KC_LCTL,         KC_LALT, KC_LGUI,  MO(4),     KC_ENT,         KC_SPC,      MO(1),    MO(2),   MO(3),    KC_RPRN
      ),

    [1] = LAYOUT_planck_2x2u(
        KC_GRV,   KC_EXLM,  KC_AT,    KC_HASH,  KC_DLR,   KC_PERC,  KC_CIRC,  KC_AMPR,  KC_ASTR,  KC_NO,    KC_NO,    KC_TRNS,
        KC_NO,    KC_BSLS,  KC_MINS,  KC_EQL,   KC_NO,    KC_NO,    KC_LEFT,  KC_DOWN,  KC_UP,    KC_RGHT,  KC_MFFD,  KC_TRNS, 
        KC_TRNS,  KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_NO,    KC_MRWD,  KC_LBRC,
        KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_NO,        KC_TRNS,           KC_TRNS,         KC_NO,    KC_NO,    KC_NO,    KC_RBRC
      ),

    [2] = LAYOUT_planck_2x2u(
        KC_TILD,  KC_1,     KC_2,     KC_3,    KC_4,   KC_5,   KC_6,      KC_7,     KC_8,   KC_9,     KC_0,     KC_TRNS,
        KC_NO,    KC_BSLS,  KC_MINS,  KC_EQL,  KC_NO,  KC_NO,  KC_LEFT,   KC_DOWN,  KC_UP,  KC_RGHT,  KC_VOLU,  KC_TRNS,
        KC_TRNS,  KC_NO,    KC_NO,    KC_NO,   KC_NO,  KC_NO,  KC_NO,     KC_NO,    KC_NO,  KC_NO,    KC_VOLD,  KC_LBRC,
        KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_NO,      KC_TRNS,          KC_TRNS,        KC_NO,  KC_NO,    KC_NO,    KC_RBRC
      ),

    [3] = LAYOUT_planck_2x2u(
        KC_NO,    KC_F1,    KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,  KC_F9,  KC_F10,  KC_DEL, 
        KC_NO,    KC_NO,    KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,  KC_NO,  KC_F15,  KC_NO, 
        KC_TRNS,  KC_NO,    KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,  KC_NO,  KC_F14,  KC_NO, 
        KC_TRNS,  KC_TRNS,  KC_TRNS,  KC_NO,       KC_TRNS,          KC_TRNS,     KC_NO,  KC_NO,  KC_NO,   QMKMACROTEST
      ),

    [4] = LAYOUT_planck_2x2u(
        MEH(KC_F1),  MEH(KC_F2),   MEH(KC_F3),   MEH(KC_F4),   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,  KC_NO,  KC_NO,
        MEH(KC_F5),  MEH(KC_F6),   MEH(KC_F7),   MEH(KC_F8),   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,  KC_NO,  KC_NO, 
        MEH(KC_F9),  MEH(KC_F10),  MEH(KC_F11),  MEH(KC_F12),  KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,  KC_NO,  KC_NO, 
        KC_NO,       KC_NO,        KC_NO,        KC_NO,              KC_NO,          KC_NO,        KC_NO,   KC_NO,  KC_NO,  KC_NO 
      ),
};

// void matrix_init_user(void) {
//   set_unicode_input_mode(UC_MAC);
// };